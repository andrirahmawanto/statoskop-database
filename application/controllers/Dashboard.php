<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model('M_dashboard');
        $this->load->helper('form','url','number');
        $this->load->library('pdf');
        $this->load->library('form_validation');
        if(empty($this->session->userdata('username'))) {
            $this->session->set_flashdata('isLogin',false);
            redirect('Login');
        }
    }  
    public function index(){
        $this->load->view('layouts/header');
        $this->load->view('layouts/sidebar');        
        $this->load->view('v_dashboard');
        $this->load->view('layouts/footer');
    }
    public function th_2021(){
        $this->session->set_userdata('tahun','2021');
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function th_2020(){
        $this->session->set_userdata('tahun','2020');
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function th_2019(){
        $this->session->set_userdata('tahun','2019');
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function th_2018(){
        $this->session->set_userdata('tahun','2018');
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function th_2017(){
        $this->session->set_userdata('tahun','2017');
        redirect($_SERVER['HTTP_REFERER']);
    }
}