<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eksplorasi extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_eksplorasi');
		$this->load->model('M_eksplorasi', 'pemain');
		$this->load->helper('form','url','number');
		$this->load->library('form_validation');
		if(empty($this->session->userdata('username'))) {
			$this->session->set_flashdata('isLogin',false);
			redirect('Login');
		}
	}  
	function klub()
	{
		$x['data'] = $this->M_eksplorasi->getKlub();
		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('v_eksplorasi_klub', $x);
		$this->load->view('layouts/footer');
	}
	function pemain(){
		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('v_eksplorasi_pemain');
		$this->load->view('layouts/footer');
	}
	public function ajax_list(){
        $list = $this->pemain->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pemain) {
            $no++;
            $row = array();
            $row[] = '<center>'.$no.'</center>';
            $row[] = $pemain->nama_pemain;
            $row[] = $pemain->posisi;
            $row[] = $pemain->nama_tim;
 
            //add html for action
            // if($this->session->userdata('level_efiling')==1){
                $row[] = '<center>
                  <a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Edit" onclick="edit_dokumen('."'".$pemain->id_pemain."'".')"><i class="fa fa-edit"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_dokumen('."'".$pemain->id_pemain."'".')"><i class="fa fa-trash"></i></a></center>';
            // }
            
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->pemain->count_all(),
                        "recordsFiltered" => $this->pemain->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
}
