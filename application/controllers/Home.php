<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct() {
    parent::__construct();
    $this->load->model('M_home');
    $this->load->helper('form','url','number','download');
    $this->load->library('form_validation');
    }    
	public function index(){        
	    $this->load->view('layouts/header_home');
		$this->load->view('v_home');
	}
  	public function post(){              
        $id_blog	    = $this->uri->segment('3'); 
	    $x['data']    	= $this->M_home->getDetailPost($id_blog);
      	$x['data1']    	= $this->M_home->getRecent();
	    $this->load->view('layouts/header_home');
		$this->load->view('v_single_post',$x);
		$this->load->view('layouts/footer_home');
	}
  	public function blog(){        
	    $x['data']    	= $this->M_home->getBlog();
      	$x['data1']    	= $this->M_home->getRecent();
	    $this->load->view('layouts/header_home');
		$this->load->view('v_all_post',$x);
		$this->load->view('layouts/footer_home');
	}
    public function about(){        
	    $this->load->view('layouts/header_home');
		$this->load->view('v_about');
		$this->load->view('layouts/footer_home');
	}
	function registration(){
		if(isset($_POST['btnsimpan']))
		{
			$id_pengguna      = $this->M_home->get_max(); 
			$password         = md5($this->input->post('password')); 
			$nama             = $this->input->post('nama'); 
			$nisn             = $this->input->post('nisn');
            $id_kelas         = $this->input->post('id_kelas');
            $id_jurusan       = $this->input->post('id_jurusan');
            $email            = $this->input->post('email');
			$level            = 3;

			$data1 = array(
				'id_pengguna'           => $id_pengguna,
				'nama_siswa'            => $nama,
				'nisn'                  => $nisn,
				'id_kelas'              => $id_kelas,
                'id_jurusan'            => $id_jurusan,
                'email'                 => $email
			);
			$this->M_home->insert_siswa($data1);
			$data2 = array(
			    'id_pengguna'       => $id_pengguna,
				'password'          => $password,
				'level'  		    => $level
			);
			$this->M_home->insert_pengguna($data2);
			redirect(site_url('home'));
		}
		else {
			redirect(site_url('home'));

		}
	}
  	public function download(){        
      force_download('assets/android/niscita.apk',NULL);
	}

}
