<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistik extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_statistik');
		$this->load->model('M_statistik', 'pemain');
		$this->load->helper('form','url','number');
		$this->load->library('form_validation');
		if(empty($this->session->userdata('username'))) {
			$this->session->set_flashdata('isLogin',false);
			redirect('Login');
		}
	}  
	function klub()
	{
		$x['data'] = $this->M_statistik->getKlub();
		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('v_statistik_klub', $x);
		$this->load->view('layouts/footer');
	}
	function pemain(){
		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('v_statistik_pemain');
		$this->load->view('layouts/footer');
	}
	public function ajax_list(){
        $list = $this->pemain->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pemain) {
            $no++;
            $row = array();
            $row[] = '<center>'.$no.'</center>';
            $row[] = $pemain->nama_pemain;
            $row[] = $pemain->posisi;
            $row[] = $pemain->nama_tim;
 
            //add html for action
            // if($this->session->userdata('level_efiling')==1){
                $row[] = '<center>
                  <a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Edit" onclick="edit_dokumen('."'".$pemain->id_pemain."'".')"><i class="fa fa-edit"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_dokumen('."'".$pemain->id_pemain."'".')"><i class="fa fa-trash"></i></a></center>';
            // }
            
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->pemain->count_all(),
                        "recordsFiltered" => $this->pemain->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	function input_tatib(){
		if(isset($_POST['btnsimpan'])){
			$id_pengguna      		= $this->M_pengguna->get_max();     
			$password		        = md5($this->input->post('password'));     
			$nama_guru              = $this->input->post('nama_guru'); 
			$nik             		= $this->input->post('nik');
			$email          		= $this->input->post('email');
			$level		            = 2;			
			$data1 = array(
				'id_pengguna'           => $id_pengguna,
				'nama_guru'             => $nama_guru,
				'nik'                   => $nik,
				'email'              	=> $email
			);
			$this->M_pengguna->insert_guru($data1);
			$data2 = array(
				'id_pengguna'       => $id_pengguna,
				'password'          => $password,
				'level'  		    => $level
			);
			$this->M_pengguna->insert_pengguna($data2);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}  
	function update_tatib(){
		if(isset($_POST['btnsimpan'])){
			$id_pengguna			= $this->input->post('id_pengguna');
			$nama_guru              = $this->input->post('nama_guru'); 
			$nik             		= $this->input->post('nik');
			$email          		= $this->input->post('email');
			
			$data = array(  
				'nama_guru'	 	 	    => $nama_guru,
				'nik'					=> $nik,
				'email'					=> $email
			);
			$this->M_pengguna->update_guru($data, $id_pengguna);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function delete_tatib(){
		if(isset($_POST['btndelete'])){
			$id = $this->input->post('id_pengguna');
			$this->M_pengguna->delete_guru($id);
			$this->M_pengguna->delete_pengguna($id);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
