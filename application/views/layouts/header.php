<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="sisbk">
    <meta name="keywords" content="sisbk">
    <meta name="author" content="sisbk">
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/home/images/icon.png">
    <!-- App title -->
    <title>STATOSKOP - Aplikasi Database Statistik</title>
    <!-- Jquery Ui -->
    <link href="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/morris/morris.css">
    
    <!--Form Wizard-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/jquery.steps/css/jquery.steps.css" />
    <link href="<?php echo base_url() ?>assets/plugins/summernote/summernote.css" rel="stylesheet" />
    <!-- App css -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/switchery/switchery.min.css">
    <!-- DataTables -->
    <link href="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
   
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">   
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />     
    <script src="<?php echo base_url() ?>assets/js/modernizr.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
</head>
<script src="<?php echo base_url() ?>assets/plugins/morris/morris.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/raphael/raphael-min.js"></script>
<body class="fixed-left">
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Top Bar Start -->
        <div class="topbar">
            <!-- LOGO -->
            <div class="topbar-left">
                <a class="logo"><span style="font-size:32px; font-weight: bold;">STATOSKOP</span><i></i></a>
                <!-- Image logo -->
                <a class="logo">
                    <span>
                        <img src="<?php echo base_url() ?>assets/images/logo/logo-sidebar.png" alt="" height="90">
                    </span>
                    <i>
                        <img src="<?php echo base_url() ?>assets/images/logo/logo-sidebar-sm.png" alt="" height="40">
                    </i>
                </a>
            </div>
            <!-- Button mobile view to collapse sidebar menu -->
            <div class="navbar navbar-default" role="navigation">
                <div class="container">
                    <!-- Navbar-left -->
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <button class="button-menu-mobile open-left waves-effect">
                                <i class="mdi mdi-tune"></i>
                            </button>
                        </li>
                    </ul>
                    <!-- Right(Notification) -->
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown navbar-c-items">
                           <a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">
                            <i class="mdi mdi-calendar"></i>
                            <!-- <span class="badge up bg-success">2022</span> -->
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-lg user-list notify-list">
                            <li class="text-center bg-success">
                                <h5><?php echo $this->session->userdata('tahun') ?></h5>
                            </li>
                            <li class="text-center">
                                <a href="<?php echo base_url().'dashboard/th_2021'?>" class="user-list-item"> 2021</a>
                            </li>
                            <li class="text-center">
                                <a href="<?php echo base_url().'dashboard/th_2020'?>" class="user-list-item"> 2020</a>
                            </li>
                            <li class="text-center">
                                <a href="<?php echo base_url().'dashboard/th_2019'?>" class="user-list-item"> 2019</a>
                            </li>
                            <li class="text-center">
                                <a href="<?php echo base_url().'dashboard/th_2018'?>" class="user-list-item"> 2018</a>
                            </li>
                            <li class="text-center">
                                <a href="<?php echo base_url().'dashboard/th_2017'?>" class="user-list-item"> 2017</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown user-box">
                        <a href="" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                            <img src="<?php echo base_url() ?>assets/images/users/user-2.png" alt="user-img" class="img-circle user-img">
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                            <li>
                                <h5><?php echo ucwords($this->session->userdata('username')) ?></h5>
                            </li>
                                    <!-- <li><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Profile</a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-lock m-r-5"></i> Lock screen</a></li> -->
                                    <li align="center"><a href="<?php echo base_url().'Login/logout'?>"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul> <!-- end navbar-right -->
                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->