<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, maximum-scale=1.0, initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />    
    <meta name="author" content="sisbk" />    
    <meta name="description" content="sisbk">
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/home/images/logo.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Document title -->
    <title>STATOSKOP - Aplikasi Database Statistik</title>
    <!-- Stylesheets & Fonts -->
    <link href="<?php echo base_url() ?>assets/home/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
</head>
  