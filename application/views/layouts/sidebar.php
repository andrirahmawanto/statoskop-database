<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu" style="padding-top: 8%">
	<div class="sidebar-inner slimscrollleft">
		<!--- Sidemenu -->
		<div id="sidebar-menu">
			<ul>
				<?php if ($this->session->userdata('level')=='1') { ?>
					<li>
						<a href="<?php echo base_url() ?>dashboard" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span></a>
					</li>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-chart-bar"></i><span> Data Statistik </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="<?php echo base_url() ?>statistik/klub">Klub</a></li>
                            <li><a href="<?php echo base_url() ?>statistik/pemain">Pemain</a></li>
                        </ul>
                    </li>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-file-find"></i><span> Data Eksplorasi </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="<?php echo base_url() ?>eksplorasi/klub">Klub</a></li>
                            <li><a href="<?php echo base_url() ?>eksplorasi/pemain">Pemain</a></li>
                        </ul>
                    </li>
                <?php } elseif ($this->session->userdata('level')=='2') {?>
                 <li>
                  <a href="<?php echo base_url() ?>dashboard" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span></a>
              </li>
              <li>
                <a href="<?php echo base_url() ?>tatib" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Tata Tertib </span></a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>dashboard/admin" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Prestasi </span></a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>dashboard/admin" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Pelanggaran </span></a>
            </li>
        <?php } elseif ($this->session->userdata('level')=='3') {?>
         <li>
            <a href="<?php echo base_url() ?>tatib" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Tata Tertib </span></a>
        </li>
        <li>
            <a href="<?php echo base_url() ?>dashboard/admin" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Prestasi </span></a>
        </li>
        <li>
            <a href="<?php echo base_url() ?>dashboard/admin" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Pelanggaran </span></a>
        </li>

    <?php } ?>
    <div class="help-box" align="center" style="margin-top: 50%">
        <h4 class="text-muted">DATABASE <?php echo $this->session->userdata('tahun') ?></h4>
    </div>

</ul>
</div>
<!-- Sidebar -->
<div class="clearfix"></div>
</div>
<!-- Sidebar -left -->
</div>
<!-- Left Sidebar End