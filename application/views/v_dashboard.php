            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">DASHBOARD</h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li class="active">
                            Dashboard
                          </li>
                        </ol>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                   <div class="col-lg-12">
                    <div class="portlet">
                      <div class="portlet-heading bg-info">
                        <h3 class="portlet-title">
                          HASIL PERTANDINGAN
                        </h3>
                        <div class="portlet-widgets">
                          <a data-toggle="collapse" data-parent="#accordion1" href="#hasil"><i class="ion-minus-round"></i></a>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                       <div id="hasil" class="panel-collapse collapse in">
                        <div class="portlet-body">
                            <table class="table table-striped table-borderless" cellspacing="0" width="100%">
                              <tbody>  
                                    <div align="center" style="margin-top: -20px">
                                      <ul class="pagination">                                        
                                        <p><b>Pekan</b></p>
                                        <li class="disabled">
                                          <a href="#"><i class="fa fa-angle-left"></i></a>
                                        </li>
                                        <li>
                                          <a href="#">1</a>
                                        </li>
                                        <li class="active">
                                          <a href="#">2</a>
                                        </li>
                                        <li>
                                          <a href="#">3</a>
                                        </li>
                                        <li>
                                          <a href="#">4</a>
                                        </li>
                                        <li>
                                          <a href="#">5</a>
                                        </li>
                                        <li>
                                          <a href="#"><i class="fa fa-angle-right"></i></a>
                                        </li>
                                      </ul>
                                    </div>                    
                                <tr>
                                  <td align="center">Persela Lamongan</td>
                                  <td>1</td>
                                  <td>-</td>
                                  <td>0</td>
                                  <td align="center">Persija Jakarta</td>
                                  <td><a href="#" class="btn btn-primary"><i class="fa fa-angle-right"></i></a></td>
                                </tr>                              
                                <tr>
                                  <td align="center">Madura United</td>
                                  <td>2</td>
                                  <td>-</td>
                                  <td>1</td>
                                  <td align="center">Persib Bandung</td>
                                  <td><a href="#" class="btn btn-primary"><i class="fa fa-angle-right"></i></a></td>
                                </tr>                              
                                <tr>
                                  <td align="center">Persikabo</td>
                                  <td>2</td>
                                  <td>-</td>
                                  <td>1</td>
                                  <td align="center">Persipura</td>
                                  <td><a href="#" class="btn btn-primary"><i class="fa fa-angle-right"></i></a></td>
                                </tr>                              
                                <tr>
                                  <td align="center">Persebaya Surabaya</td>
                                  <td>2</td>
                                  <td>-</td>
                                  <td>1</td>
                                  <td align="center">Persik Kediri</td>
                                  <td><a href="#" class="btn btn-primary"><i class="fa fa-angle-right"></i></a></td>
                                </tr>                              
                                <tr>
                                  <td align="center">Persiraja</td>
                                  <td>2</td>
                                  <td>-</td>
                                  <td>1</td>
                                  <td align="center">Barito Putera</td>
                                  <td><a href="#" class="btn btn-primary"><i class="fa fa-angle-right"></i></a></td>
                                </tr>
                                <tr>
                                  <td align="center">Persiraja</td>
                                  <td>2</td>
                                  <td>-</td>
                                  <td>1</td>
                                  <td align="center">Barito Putera</td>
                                  <td><a href="#" class="btn btn-primary"><i class="fa fa-angle-right"></i></a></td>
                                </tr>
                                <tr>
                                  <td align="center">Persiraja</td>
                                  <td>2</td>
                                  <td>-</td>
                                  <td>1</td>
                                  <td align="center">Barito Putera</td>
                                  <td><a href="#" class="btn btn-primary"><i class="fa fa-angle-right"></i></a></td>
                                </tr>
                                <tr>
                                  <td align="center">Persiraja</td>
                                  <td>2</td>
                                  <td>-</td>
                                  <td>1</td>
                                  <td align="center">Barito Putera</td>
                                  <td><a href="#" class="btn btn-primary"><i class="fa fa-angle-right"></i></a></td>
                                </tr>
                                <tr>
                                  <td align="center">Persiraja</td>
                                  <td>2</td>
                                  <td>-</td>
                                  <td>1</td>
                                  <td align="center">Barito Putera</td>
                                  <td><a href="#" class="btn btn-primary"><i class="fa fa-angle-right"></i></a></td>
                                </tr>
                              </tbody>           
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row" style="margin-top: 5px">
                   <div class="col-lg-12">
                    <div class="portlet">
                      <div class="portlet-heading bg-info">
                        <h3 class="portlet-title">
                          STATISTIK KOMPETISI
                        </h3>
                        <div class="portlet-widgets">
                          <a data-toggle="collapse" data-parent="#accordion2" href="#statistik"><i class="ion-minus-round"></i></a>                                   
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div id="statistik" class="panel-collapse collapse in">
                        <div class="portlet-body">
                         <div class = "row">
                          <div class="col-lg-3 col-md-6" align="center">
                            <div class="card-box widget-box-two widget-two-primary">
                              <div class="wigdet-two-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">GOL</p>
                                <h2><span data-plugin="counterup">20</span> </h2>
                              </div>
                            </div>
                          </div><!-- end col -->

                          <div class="col-lg-3 col-md-6" align="center">
                            <div class="card-box widget-box-two widget-two-primary">
                              <div class="wigdet-two-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">GOL</p>
                                <h2><span data-plugin="counterup">20</span> </h2>
                              </div>
                            </div>
                          </div><!-- end col -->

                          <div class="col-lg-3 col-md-6" align="center">
                            <div class="card-box widget-box-two widget-two-primary">
                              <div class="wigdet-two-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">GOL</p>
                                <h2><span data-plugin="counterup">20</span> </h2>
                              </div>
                            </div>
                          </div><!-- end col -->

                          <div class="col-lg-3 col-md-6" align="center">
                            <div class="card-box widget-box-two widget-two-primary">
                              <div class="wigdet-two-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">GOL</p>
                                <h2><span data-plugin="counterup">20</span> </h2>
                              </div>
                            </div>
                          </div><!-- end col -->

                          <div class="col-lg-3 col-md-6" align="center">
                            <div class="card-box widget-box-two widget-two-primary">
                              <div class="wigdet-two-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">GOL</p>
                                <h2><span data-plugin="counterup">20</span> </h2>
                              </div>
                            </div>
                          </div><!-- end col -->

                          <div class="col-lg-3 col-md-6" align="center">
                            <div class="card-box widget-box-two widget-two-primary">
                              <div class="wigdet-two-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">GOL</p>
                                <h2><span data-plugin="counterup">20</span> </h2>
                              </div>
                            </div>
                          </div><!-- end col -->

                          <div class="col-lg-3 col-md-6" align="center">
                            <div class="card-box widget-box-two widget-two-primary">
                              <div class="wigdet-two-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">GOL</p>
                                <h2><span data-plugin="counterup">20</span> </h2>
                              </div>
                            </div>
                          </div><!-- end col -->

                          <div class="col-lg-3 col-md-6" align="center">
                            <div class="card-box widget-box-two widget-two-primary">
                              <div class="wigdet-two-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">GOL</p>
                                <h2><span data-plugin="counterup">20</span> </h2>
                              </div>
                            </div>
                          </div><!-- end col -->

                          <div class="col-lg-3 col-md-6" align="center">
                            <div class="card-box widget-box-two widget-two-primary">
                              <div class="wigdet-two-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">GOL</p>
                                <h2><span data-plugin="counterup">20</span> </h2>
                              </div>
                            </div>
                          </div><!-- end col -->

                          <div class="col-lg-3 col-md-6" align="center">
                            <div class="card-box widget-box-two widget-two-primary">
                              <div class="wigdet-two-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">GOL</p>
                                <h2><span data-plugin="counterup">20</span> </h2>
                              </div>
                            </div>
                          </div><!-- end col -->
                          
                          <div class="col-lg-3 col-md-6" align="center">
                            <div class="card-box widget-box-two widget-two-primary">
                              <div class="wigdet-two-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">GOL</p>
                                <h2><span data-plugin="counterup">20</span> </h2>
                              </div>
                            </div>
                          </div><!-- end col -->

                          <div class="col-lg-3 col-md-6" align="center">
                            <div class="card-box widget-box-two widget-two-primary">
                              <div class="wigdet-two-content">
                                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">GOL</p>
                                <h2><span data-plugin="counterup">20</span> </h2>
                              </div>
                            </div>
                          </div><!-- end col -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
