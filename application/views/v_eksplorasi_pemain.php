            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">DATA EKSPLORASI PEMAIN</h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li class="active">
                          </li>
                        </ol>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <div id="bg-default2" class="panel-collapse collapse in">
                    <div class="portlet-body">
                      <div class="col-md-12">
                        <ul class="nav nav-tabs tabs-bordered nav-justified">
                          <li class="active">
                            <a href="#home-b2" data-toggle="tab" aria-expanded="true">
                              <span class="visible-xs"><i class="fa fa-home"></i></span>
                              <span class="hidden-xs">Bulan</span>
                            </a>
                          </li>
                          <li class="">
                            <a href="#profile-b2" data-toggle="tab" aria-expanded="false">
                              <span class="visible-xs"><i class="fa fa-user"></i></span>
                              <span class="hidden-xs">Pekan</span>
                            </a>
                          </li>

                        </ul>
                        <div class="tab-content">
                          <div class="tab-pane active" id="home-b2">
                            <div class = "row">
                              <div class="col-sm-12">
                                <div class="col-md-12" align="center">
                                  <form class="form-horizontal" method="POST" action="">
                                    <?php
                                    $aksi2= array('Goal', 'Intercept', 'Aerial Duel', 'Assist', 'Clearence','Yellow Card', 'Red Card', 'Dribble', 'Keypass', 'Melanggar', 'Offside', 'Tackle', 'Recovery', 'Shot', 'Pass', 'Cross', 'Saves');
                                    $bulan = array('3' => 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember' );
                                    ?>

                                    <div class="col-sm-1">
                                    </div>
                                    <div class="col-sm-2">
                                    <select class="form-control" data-style="btn-default" name="pilihanfilter4" required="">
                                      <option value="<?php null ?>">Pilih Aksi</option>
                                      <?php
                                      foreach($aksi2 as $item){
                                        ?>
                                        <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
                                        <?php
                                      }
                                      ?>
                                    </select>
                                  </div>
                                  <div class="col-sm-2">
                                    <select class="form-control" data-style="btn-default" name="pilihanfilter5">
                                      <option value="<?php null ?>">Pilih Parameter</option>
                                      <option>Success</option>
                                      <option>Failed</option>
                                    </select>
                                  </div>
                                  <div class="col-sm-2">
                                    <select class="form-control" data-style="btn-default" name="pilihanfilter7">
                                      <option value="<?php null ?>">Pilih Posisi</option>
                                      <option>PENJAGA GAWANG</option>
                                      <option>BEK</option>
                                      <option>GELANDANG</option>
                                      <option>PENYERANG</option>
                                    </select>
                                  </div>
                                  <div class="col-sm-2">
                                    <select class="form-control" data-style="btn-default" name="pilihanfilter6" required="">
                                      <option value="<?php null ?>">Pilih Bulan</option>
                                      <?php
                                      $angkaBulan=3;
                                      foreach($bulan as $item2){
                                        ?>
                                        <option value="<?php echo $angkaBulan; ?>"><?php echo $item2; ?></option>
                                        <?php
                                        $angkaBulan++;
                                      }
                                      ?>
                                    </select>
                                  </div>
                                  <div class="col-sm-2">
                                    <input type="submit" class="btn btn-info btn-submit" name="cari2" value="Cari">
                                  </div>
                                  </form>
                                </div> 
                        <div align="center" style="margin-top: 50px">
                                <?php
                                if (isset($_POST['cari2'])) {
                                  $filter4            = $_POST['pilihanfilter4'];
                                  $filter5            = $_POST['pilihanfilter5'];
                                  $filter6            = $_POST['pilihanfilter6'];
                                  $filter7            = $_POST['pilihanfilter7'];
                                  ?>
                                  <div class="col-md-12" align="center" style="font-size: 18; ">
                                    <?php 
                                    if ($filter7=='') {
                                      ?><div class="btn btn-primary"><i class="fa fa-filter fa-1x"></i>  : </div>     <div class="btn btn-warning"><?php echo "  "."$filter4"." "."$filter5"." "."Bulan "."$bulan[$filter6]";?></div><?php
                                    }
                                    else{
                                      ?><div class="btn btn-primary"><i class="fa fa-filter fa-1x"></i>  : </div>     <div class="btn btn-warning"><?php echo "  "."$filter4"." "."$filter5"." "."Bulan "."$bulan[$filter6]";?></div> <div class="btn btn-primary"><i class="fa fa-user fa-1x"></i>  : </div>   <div class="btn btn-warning"><?php echo "  "."$filter7";?></div><?php
                                    }
                                    ?>
                                  </div>
                                  <?php
                                } else {
                                  ?>
                                  <div class="col-md-12" align="center" style="font-size: 18; ">
                                    <div class="btn btn-primary"><i class="fa fa-filter fa-1x"></i>  : </div> <div class="btn btn-danger">
                                      <?php
                                      echo "Tidak Ada Filter Yang Dipilih !!";
                                      ?></div>
                                      <?php
                                    }
                                    ?>
                                  </div>
                                </div>
                                </div>
                              </div> 
                            </div>
                            <div class="tab-pane" id="profile-b2">
                              <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
                            </div>
                          </div>
                        </div> <!-- end col -->
                      </div>
                      <!-- end row -->
                    </div>
                  </div>
                  <br>



                  <script type="text/javascript">
                    $(document).ready(function(){
                      $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                        localStorage.setItem('activeTab', $(e.target).attr('href'));
                      });
                      var activeTab = localStorage.getItem('activeTab');
                      if(activeTab){
                        $('#myTab a[href="' + activeTab + '"]').tab('show');
                      }
                    });
                  </script>
                  <script>
                    $(document).ready(function() {
                      var oTable = $('#tb_tim_terbaik').dataTable();
                      oTable.fnSort( [ [1,'desc'] ] );
                    } );
                  </script>
                  <script>
                    $(document).ready(function() {
                      var oTable = $('#tb_pemain_terbaik').dataTable();
                      oTable.fnSort( [ [2,'desc'] ] );
                    } );
                  </script>
                  <script>
                    $(document).ready(function() {
                      var oTable = $('#tb_pemain_terbaik_mingguan').dataTable();
                      oTable.fnSort( [ [2,'desc'] ] );
                    } );
                  </script>

                </div>
                <!-- END wrapper -->
                <script type="text/javascript">
                  $(document).ready(function() {
                    $('#kelas').DataTable({
                      "language": {
                        "lengthMenu": 'Tampilkan _MENU_ Baris',
                        "search": '_INPUT_',
                        "searchPlaceholder": 'Pencarian . . .'
                      },
                      dom: 
                      "<'row'<'col-md-8'l><'col-md-2 pull-right'f> <'col-md- pull-right' B>>" +
                      "<'row'<'col-md-12't>>" +
                      "<'row'<'col-md-5'i><'col-md-7'p>>",
                      buttons: [
                      {
                        "text":'<i class="fa fa-plus-circle"></i> | Tambah',"className": 'btn btn-success btn-sm',
                        action: function ( e, dt, node, config ) {
                          var selected = dt.row( { selected: true } ).data();
                          $('#ModalAdd').modal('show');
                        }
                      }
                      ]
                    });
                  } );  
                </script>