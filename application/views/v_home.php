<body>
  <div class="container">
    <div class="row px-3">
      <div class="col-lg-10 col-xl-9 card flex-row mx-auto px-0">
        <div class="img-left d-none d-md-flex"></div>
        <div class="card-body">
          <h4 class="title text-center mt-4">
            <b>HALAMAN LOGIN</b>
          </h4>
          <form class="form-box px-3" action="<?php echo base_url().'login/cek_login'?>" method="post" role="form">
            <div class="form-input">
              <span><i class="fa fa-user"></i></span>
              <input type="text" name="username" placeholder="Username" tabindex="10" required>
            </div>
            <div class="form-input">
              <span><i class="fa fa-lock"></i></span>
              <input type="password" name="password" placeholder="Password" required>
            </div>
            <div class="mb-3">
              <button type="submit" class="btn btn-block text-uppercase">
                Login
              </button>
            </div>
            <hr class="my-4">

            <div class="text-center mb-2">
              <strong> &copy; 2022 | STATOSKOP DATABASE </strong>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</body>
