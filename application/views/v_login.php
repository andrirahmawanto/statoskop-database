<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Checklist CHSE Berbasis Web PT. Surveyor Indonesia">
    <meta name="keywords" content="CHSE, chse, Cleanliness Healthiness Safety and Environment, sisub, ptsi, Surveyor Indonesia">
    <meta name="author" content="tim it sisub">

    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.ico">
    <!-- App title -->
    <title>CHSESI - Cleanliness Healthiness Safety and Environment</title>

    <!-- App css -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url() ?>assets/js/modernizr.min.js"></script>
    <style type="text/css">
        body{
            /*background-image: url('<?php echo base_url() ?>assets/images/background.png');*/
            background: url('<?php echo base_url() ?>assets/images/login-back-fix.jpg');
            background-repeat: no-repeat;
            -webkit-background-size: 100% 100%;
            -moz-background-size: 100% 100%;
            -o-background-size: 100% 100%;
            background-size: 100% 100%;
        }
        .chatWa {
            width: 40px;
            height: 40px;
            line-height: 1px;
            display: inline-block;
            color: #fff;
            position: fixed;
            border-radius: 100%;
            bottom: 25px;
            right: 35px;
            z-index: 100;
            text-align: center;
            font-size: 18px;
        }
        .chatWa:focus {
            border: 1px solid #fff;
            color: #fff;
        }
    </style>
</head>
<body class="bg-transparent">
    <!-- HOME -->
    <section>
        <div class="container-alt" style="padding-top: 8em">
            <div class="row">
                <div class="col-sm-12">
                    <div class="wrapper-page">
                        <div class="m-t-40 account-pages">
                            <div class="text-center account-logo-box" style="background-color: #cfcacd;">
                                <h2 class="text-uppercase">
                                    <a class="text-success">
                                        <span><img src="<?php echo base_url() ?>assets/images/logo/logo.png" alt="" width="85%"></span>
                                    </a>
                                </h2>
                            </div>
                            <div class="account-content" style="background-color: white;">
                                <form class="form-horizontal" action="<?php echo base_url().'login/cek_login'?>" method="post" role="form">

                                    <div class="form-group ">
                                        <div class="col-xs-12">
                                            <input class="form-control" name="username" type="text" required="" placeholder="Username">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <input class="form-control" name="password" type="password" required="" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="form-group account-btn text-center m-t-10">
                                        <div class="col-xs-12">
                                            <a href="<?php echo base_url() ?>daftar" class="col-md-6 btn w-md btn-bordered btn-success waves-effect waves-light">Form Pendaftaran</a>
                                            <button class="col-md-6 btn w-lg  btn-bordered btn-primary waves-effect waves-light" type="submit">Login</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!-- end card-box-->
                    </div>
                    <!-- end wrapper -->
                </div>
            </div>
        </div>
    </section>
    <!-- END HOME -->
    <!-- <a href="https://api.whatsapp.com/send?phone=6285733223979&text=Halo%20admin%20saya%20ingin%20menanyakan" target="_blank" class="chatWa">
        <button type="button" class="btn btn-lg btn-success" style="border-radius: 50%"><i class="mdi mdi-24px mdi-whatsapp"></i></button>
    </a> -->
</body>
</html>
