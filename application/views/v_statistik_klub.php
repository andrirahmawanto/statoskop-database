            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">DAFTAR KLUB</h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li class="active">
                          </li>
                        </ol>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  
                  <div id="bg-default2" class="panel-collapse collapse in">
                    <div class="portlet-body">
                      <div class = "row">
                        <div class="col-sm-12">
                          <?php foreach ($data as $rowdata) { ?>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                              <div align="center">
                                <a href=""><img src="<?php echo base_url() ?>assets/images/logo-klub/<?php echo $rowdata->logo ?>" height="100" width ="100"/></a><br>
                                <span class="info-box-text" style="font-weight: bold; font-size: 12px;" ><a href=""><?php echo $rowdata->nama_tim ?></a></span><br>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                          <?php } ?>
                        </div>
                      </div> 
                    </div>
                  </div>
                </div>
                <!-- END wrapper -->
                <script type="text/javascript">
                  $(document).ready(function() {
                    $('#kelas').DataTable({
                      "language": {
                        "lengthMenu": 'Tampilkan _MENU_ Baris',
                        "search": '_INPUT_',
                        "searchPlaceholder": 'Pencarian . . .'
                      },
                      dom: 
                      "<'row'<'col-md-8'l><'col-md-2 pull-right'f> <'col-md- pull-right' B>>" +
                      "<'row'<'col-md-12't>>" +
                      "<'row'<'col-md-5'i><'col-md-7'p>>",
                      buttons: [
                      {
                        "text":'<i class="fa fa-plus-circle"></i> | Tambah',"className": 'btn btn-success btn-sm',
                        action: function ( e, dt, node, config ) {
                          var selected = dt.row( { selected: true } ).data();
                          $('#ModalAdd').modal('show');
                        }
                      }
                      ]
                    });
                  } );  
                </script>