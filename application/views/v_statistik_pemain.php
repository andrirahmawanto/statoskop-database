            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">DAFTAR PEMAIN</h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li class="active">
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
                  <!-- Modal Add-->
                <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                      <form method="post" action="<?php echo site_url('kelola/input_kelas');?>" enctype='multipart/form-data'>
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">TAMBAH DATA KELAS</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Nama Kelas</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="text" class="form-control" id="nama_kelas" name="nama_kelas" placeholder="Masukkan Kelas" required>
                           </div>
                         </div>
                       <div class="modal-footer" style="margin-top: 15%">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <!-- END Modal -->
  <div id="bg-default2" class="panel-collapse collapse in">
    <div class="portlet-body">
      <div class = "row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table id="table" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 10%">Nama Pemain</th>
                            <th style="text-align: center; width: 1%">Posisi</th>
                            <th style="text-align: center; width: 8%">Klub</th>                
                            <th style="text-align: center; width: 1%">Fitur</th>
                        </tr>
                    </thead>
                    <tbody>        
                </tbody>
            </table>
        </div>
    </div>
</div> 
</div>
</div>
</div>

<script>
var save_method; //for save method string
var table;
var base_url = '<?php echo base_url();?>';

$(document).ready(function() {
 
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('statistik/ajax_list')?>",
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
        "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
        dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f> <'col-md- pull-right' B>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>",
            buttons: [
            
            ]
    });
  
});
function add_doc(){
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Surat Keluar'); // Set Title to Bootstrap modal title
}
function edit_dokumen(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('dokumen/ajax_edit2/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id_surat_keluar"]').val(data.id_surat_keluar);
            $('[name="tgl_surat"]').val(data.tgl_surat);
            $('[name="no_surat"]').val(data.no_surat);
            $('[name="perihal"]').val(data.perihal);
            $('[name="pengirim"]').val(data.id_pengirim);
            $('[name="penerima"]').val(data.id_penerima);
            $('[name="cc"]').val(data.id_cc);
            $('[name="sifat_surat"]').val(data.id_sifat_surat);
            $('[name="tipe_surat"]').val(data.id_tipe_surat);
            $('[name="topik"]').val(data.topik);
            $('[name="kegiatan"]').val(data.kegiatan);
            $('[name="pic"]').val(data.id_pic);
            $('[name="due_date"]').val(data.due_date);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Surat Keluar'); // Set title to Bootstrap modal title
            $('#photo-preview').show(); // show photo preview modal
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo site_url('dokumen/ajax_add2')?>";
    } else {
        url = "<?php echo site_url('dokumen/ajax_update2')?>";
    }
 
    // ajax adding data to database
   var formData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {
 
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
 
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}
function delete_dokumen(id)
{
    if(confirm('Apakah anda yakin akan menghapus data tersebut ?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('dokumen/ajax_delete2')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();                
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        }); 
    }
}
</script>
<script>
  jQuery(document).ready(function(){
    $('.summernote').summernote({toolbar: [
    ['fontname', ['fontname']],
    ['font', ['bold', 'italic', 'underline']],
    ['para', ['ul']]],
        height: 300,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false                 // set focus to editable area after initializing summernote
    });
    $('.inline-editor').summernote({airMode: true});
    });
</script>
              
              
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Tambah Surat Keluar</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" name="form" class="form-horizontal">
                <div class="modal-body" style="font-family: sans-serif; font-size: 12px;">
                  <input type="hidden" name="id_surat_keluar"/>
                   <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Tanggal Surat</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="date" class="form-control" id="tgl_surat" name="tgl_surat" required>
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">No Surat</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="text" class="form-control" id="no_surat" name="no_surat" placeholder="Masukkan No Surat" required>
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Perihal</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="text" class="form-control" id="perihal" name="perihal" placeholder="Masukkan Perihal" required>
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Pengirim</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                               <select class="form-control" id="pengirim" name="pengirim">
                                    <option value="">Pilih Nama Pengirim</option>
                                        <?php foreach ($data as $rowdata) { ?>
                                            <option value="<?php echo $rowdata->id_unit_kerja ?>"><?php echo $rowdata->nama_unit_kerja ?></option>
                                        <?php }?>
                                </select>
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Penerima</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <select class="form-control" id="penerima" name="penerima">
                                    <option value="">Pilih Nama Penerima</option>
                                        <?php foreach ($data1 as $rowdata) { ?>
                                            <option value="<?php echo $rowdata->id_divisi ?>"><?php echo $rowdata->nama_divisi ?></option>
                                        <?php }?>
                                </select>
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Cc</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                               <select class="form-control" id="cc" name="cc">
                                    <option value="">Pilih Nama CC</option>
                                        <?php foreach ($data2 as $rowdata) { ?>
                                            <option value="<?php echo $rowdata->id_divisi ?>"><?php echo $rowdata->nama_divisi ?></option>
                                        <?php }?>
                                </select>
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Sifat Surat</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                                <select class="form-control" id="sifat_surat" name="sifat_surat">
                                    <option value="">Pilih Sifat Surat</option>
                                        <?php foreach ($data3 as $rowdata) { ?>
                                            <option value="<?php echo $rowdata->id_sifat_surat ?>"><?php echo $rowdata->sifat_surat ?></option>
                                        <?php }?>
                                </select>
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Tipe Surat</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                               <select class="form-control" id="tipe_surat" name="tipe_surat">
                                    <option value="">Pilih Tipe Surat</option>
                                        <?php foreach ($data4 as $rowdata) { ?>
                                            <option value="<?php echo $rowdata->id_tipe_surat ?>"><?php echo $rowdata->tipe_surat ?></option>
                                        <?php }?>
                                </select>
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Topik</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                            <textarea class="form-control" id="topik" name="topik" rows="3" placeholder="Masukkan Topik"></textarea>
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Kegiatan</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                               <textarea class="form-control" id="kegiatan" name="kegiatan" rows="5" placeholder="Masukkan Kegiatan"></textarea>
                               <!--<textarea class="summernote" id="kegiatan" name="kegiatan" rows="2" cols="30"></textarea>-->
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Nama PIC</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                               <select class="form-control" id="pic" name="pic">
                                    <option value="">Pilih Nama PIC</option>
                                        <?php foreach ($data5 as $rowdata) { ?>
                                            <option value="<?php echo $rowdata->id_pic ?>"><?php echo $rowdata->nama_pic ?></option>
                                        <?php }?>
                                </select>
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Due Date</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="date" class="form-control" id="due_date" name="due_date" required>
                           </div>
                </div>
                </form>
            </div>
            <div class="modal-footer" style="margin-top: 80%">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                <button type="button" id="btnSave" onclick="save()"  class="btn btn-success">Simpan</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->